<?php

function fib($number)
{
    $fib = array();
    
    if ($number > 0) {
        for ($i = 0; $i < $number; $i++) {
            if ($i <= 1) {
                $fib[] = $i;
            } else {
                $fib[] = $fib[$i - 1] + $fib[$i - 2];
            }
        }
    }
    
    return $fib;
}
