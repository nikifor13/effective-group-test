<?php

header('Content-type: text/plain; charset=utf-8');

require "fib.php";

$fib = fib(64);

foreach ($fib as $number) {
    echo "$number\n";
}
