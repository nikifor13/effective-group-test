<?php

require "circle.php";
require "rectangle.php";
require "pyramid.php";

function generateFigures()
{
    $figures = array();
    $figuresNumber = rand(10, 100);
    
    for ($i = 0; $i < $figuresNumber; $i++) {
        $figureType = rand(1, 99);
        
        if ($figureType <= 33) {
            $radius = rand(1, 100);
            $figures[] = new Circle($radius);
        } elseif ($figureType > 33 and $figureType <=66) {
            $width = rand(1, 100);
            $height = rand(1, 100);
            $figures[] = new Rectangle($width, $height);
        } else if ($figureType > 66) {
            $height = rand(1, 50);
            $sidesNumber = rand(3, 15);
            $sideLength = rand(1, 10);
            $figures[] = new RegularPyramid($height, $sidesNumber, $sideLength);
        }
    }

    return $figures;
}

function compareByPlaneArea($a, $b)
{
    $aArea = $a->planeArea();
    $bArea = $b->planeArea();
    
    if ($aArea == $bArea) {
        return 0;
    }
    return ($aArea < $bArea) ? 1 : -1;
}

function sortFigures(&$figuresArray)
{
    usort($figuresArray, "compareByPlaneArea");
}

function saveFiguresArray($figuresArray)
{
    $arrayToSave = serialize($figuresArray);
    $fileName = "figures.fg";
    $file = fopen($fileName, "wb");
    fwrite($file, $arrayToSave);
    fclose($file);
}

function loadFiguresArray()
{
    $fileName = "figures.fg";
    $file = fopen($fileName, "rb");
    if ($file == false) {
        return 0;
    } else {
        $arrayToLoad = fread($file, filesize($fileName));
        fclose($file);
        $figuresArray = unserialize($arrayToLoad);
        return $figuresArray;
    }
}
