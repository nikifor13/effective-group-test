<?php

class Rectangle
{
    private $width;
    private $height;
    
    function __construct($width, $height)
    {
        if ($width <= 0) {
            $this->width = 1;
        } else {
            $this->width = $width;
        }
        
        if ($height <= 0) {
            $this->height = 1;
        } else {
            $this->height = $height;
        }
    }
    
    public function planeArea()
    {
        $area = $this->width * $this->height;
        return $area;
    }
}
