<?php

class RegularPyramid
{
    private $height;
    private $sidesNumber;
    private $sideLength;
    
    function __construct($height, $sidesNumber, $sideLength)
    {
        if ($height <= 0) {
            $this->height = 1;
        } else {
            $this->height = $height;
        }
        
        if ($sidesNumber < 3) {
            $this->sidesNumber = 3;
        } else {
            $this->sidesNumber = $sidesNumber;
        }
        
        if ($sideLength <= 0) {
            $this->sideLength = 1;
        } else {
            $this->sideLength = $sideLength;
        }
    }
    
    public function planeArea()
    {
        return 0;
    }
    
    public function baseArea()
    {
        $baseArea = ($this->sidesNumber * pow($this->sideLength, 2)) /
            (4 * tan(M_PI / $this->sidesNumber));
        return $baseArea;
    }
    
    public function sideArea()
    {
        $apothem = sqrt(pow($this->height, 2) +
            pow($this->sideLength / (2 * tan(M_PI / $this->sidesNumber)), 2));
        $sideArea = ($this->sideLength * $this->sidesNumber * $apothem) / 2;
        return $sideArea;
    }
    
    public function surfaceArea()
    {
        $baseArea = $this->baseArea();
        $sideArea = $this->sideArea();
        $surfaceArea = $baseArea + $sideArea;
        return $surfaceArea;
    }
}
