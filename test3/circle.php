<?php

class Circle
{
    private $radius;
    
    function __construct($radius)
    {
        if ($radius <= 0) {
            $this->radius = 1;
        } else {
            $this->radius = $radius;
        }
    }
    
    public function planeArea()
    {
        $area = M_PI * pow($this->radius, 2);
        return $area;
    }
}
