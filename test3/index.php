<?php

header('Content-type: text/plain; charset=utf-8');

require "figureutils.php";

echo "Generaring figures...\n\n";

$figures = generateFigures();

foreach($figures as $figure) {
    $area = (int) $figure->planeArea();
    $name = get_class($figure);
    echo "Plane area: $area Type: $name\n";
}

saveFiguresArray($figures);

$figures = loadFiguresArray();

sortFigures($figures);

echo "\nSorting by plane area decreasing...\n\n";

foreach($figures as $figure) {
    $area = (int) $figure->planeArea();
    $name = get_class($figure);
    echo "Plane area: $area Type: $name\n";
}
