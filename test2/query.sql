select first_name, last_name
from authors
where author_id in (
	select author_id
	from (
		select author_id, count(book_id) as books_number
		from publications
		group by author_id
	)
	where books_number < 7
);